from math import prod
from itertools import *
from time import time

def find_product(n, xs):
    return prod([ys for ys in combinations(xs, n) if sum(ys) == 2020][0])

if __name__ == '__main__':
    start = time()

    nums = list(map(int, open("data.txt")))
    for n in [2,3]:
        print(find_product(n, nums))

    print(time() - start)

