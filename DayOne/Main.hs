module Main where

import Control.Monad
import Data.List

findProduct n = fmap product . find ((== 2020) . sum) . replicateM n

-- >>> 3 + 7
main :: IO ()
main = do
  nums <- map read . lines <$> readFile "data.txt"
  forM_ [2,3] (print . (`findProduct` nums))

