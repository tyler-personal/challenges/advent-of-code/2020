data Partition = Range Int Int | Final Int
data Choice = Up | Down

partition : Partition -> Choice -> Partition
partition (Range x y) choice = case choice of
    Up   => Range (x + halfDiff) y
    Down => Range x (y - halfDiff)
  where
    halfDiff : Int
    halfDiff = (y - x + 1) `div` 2

getIfEqual : Partition -> Maybe Int
getIfEqual (Range x y) = case x == y of
  True => Just x
  _ => Nothing

parse : (Int, Int) -> List Choice -> Maybe Int
parse (x,y) = getIfEqual . foldl partition (Range x y)

toChoice : Char -> Maybe Choice
toChoice 'F' = Just Down
toChoice 'B' = Just Up
toChoice 'L' = Just Down
toChoice 'R' = Just Up
toChoice _   = Nothing

toSeatID : String -> Maybe Int
toSeatID s = pure ((!row * 8) + !col)
  where
    row = parse (0,127) . mapMaybe toChoice . take 7 $ unpack s
    col = parse (0,7) . mapMaybe toChoice . drop 7 $ unpack s

main : IO ()
main = do
  seatIDs <- (mapMaybe toSeatID . either (const []) lines) <$> readFile "data.txt"

  let min = foldl1 min seatIDs
  let max = foldl1 max seatIDs

  let missingIDs = filter (not . (`elem` seatIDs)) [min .. max]
  printLn missingIDs

  putStrLn $ "Part 1: " ++ show max
