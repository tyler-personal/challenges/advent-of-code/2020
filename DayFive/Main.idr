%default total

Range : Type
Range = (Int, Int)

data Choice = Up | Down

partition : Range -> Choice -> Range
partition (x,y) choice = case choice of
    Up   => (x + halfDiff, y)
    Down => (x, y - halfDiff)
  where
    halfDiff : Int
    halfDiff = cast {to=Int} $ cast {to=Double} (y - x + 1) / 2

getIfEqual : Range -> Maybe Int
getIfEqual (x,y) = case x == y of
  True => Just x
  _ => Nothing

parse : Range -> List Choice -> Maybe Int
parse r = getIfEqual . foldl partition r

toChoice : Char -> Maybe Choice
toChoice 'F' = Just Down
toChoice 'B' = Just Up
toChoice 'L' = Just Down
toChoice 'R' = Just Up
toChoice _   = Nothing

toSeatID : String -> Maybe Int
toSeatID s = pure ((!row * 8) + !col)
  where
    row = parse (0,127) . mapMaybe toChoice . take 7 $ unpack s
    col = parse (0,7) . mapMaybe toChoice . drop 7 $ unpack s

main : IO ()
main = do
  seatIDs <- (mapMaybe toSeatID . either (const []) lines) <$> readFile "data.txt"

  let max = foldl max 0 seatIDs
  let min = foldl min max seatIDs

  let missingIDs = filter (not . (`elem` seatIDs)) $ [min .. max]
  let myID = find (\n => (n + 1 `elem` seatIDs) && (n - 1 `elem` seatIDs)) missingIDs

  putStrLn $ "Part 1: " ++ show max
  putStrLn $ "Part 2: " ++ show myID

