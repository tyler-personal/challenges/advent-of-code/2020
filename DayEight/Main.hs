{-# LANGUAGE LambdaCase, TemplateHaskell, RecordWildCards #-}

import Control.Lens hiding (index)
import Data.List
import Debug.Trace
import Control.Arrow

data Instruction = Acc Int | Jmp Int | Nop Int
  deriving (Eq, Show)

data State = State { _accumulator :: Int, _index :: Int, _indexes :: [Int], _instructions :: [Instruction] }
  deriving (Eq, Show)

makeLenses ''State

process :: State -> State
process state@State{..} = trace' state
  & indexes %~ (<> [_index])
  & case _instructions !! _index of
    Acc x -> index %~ (+ 1) >>> accumulator %~ (+ x)
    Jmp x -> index %~ (+ x)
    Nop _ -> index  %~ (+ 1)

trace' x = trace (show x) x

part1 :: State -> State
part1 state = last $ takeWhile (\d -> trace' (_indexes d) == trace' (nub (_indexes d))) (iterate process state)

readInstruction :: String -> Instruction
readInstruction str = maybeNegate num & case op of
  "nop" -> Nop
  "jmp" -> Jmp
  "acc" -> Acc
  where
    (op,sign,num) = (take 3 str, str !! 4, read (drop 5 str))
    maybeNegate n = if sign == '-' then negate n else n

main :: IO ()
main = do
  state <- State 0 0 [] . map readInstruction . lines <$> readFile "data.txt"
  print $ part1 state

