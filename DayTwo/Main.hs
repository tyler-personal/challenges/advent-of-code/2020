{-# LANGUAGE RecordWildCards #-}
import Data.List.Split
import Control.Monad
import Data.Composition

data Password = Password
  { minCount :: Int
  , maxCount :: Int
  , requiredChar :: Char
  , text :: String
  } deriving Show

mkPassword :: String -> Password
mkPassword str = Password (read x) (read y) (head char) text
  where [x,y,char,text] = filter (not . null) . splitOneOf "-: " $ str

validPasswordPart1 :: Password -> Bool
validPasswordPart1 Password{..} = charCount `elem` [minCount..maxCount]
  where charCount = count (== requiredChar) text

validPasswordPart2 :: Password -> Bool
validPasswordPart2 Password {..} = (x == requiredChar) /= (y == requiredChar)
  where (x,y) = (text !! (minCount - 1), text !! (maxCount - 1))

count :: (a -> Bool) -> [a] -> Int
count = length .: filter

main :: IO ()
main = do
  passwords <- map mkPassword . lines <$> readFile "data.txt"
  forM_ [validPasswordPart1, validPasswordPart2] $ print . (`count` passwords)
