import Data.List.Split
import Control.Monad

main = do
  passwords <- map ((\[x,y,c,s] -> (read x, read y, head c, s)) . filter (not . null) . splitOneOf "-: ") . lines <$> readFile "data.txt"
  let part1 (x,y,c,s) = length (filter (== c) s) `elem` [x..y]
  let part2 (x,y,c,s) = ((s !! (x - 1)) == c) /= ((s !! (y - 1)) == c)
  forM_ [part1, part2] $ \p -> print . length $ filter p passwords
