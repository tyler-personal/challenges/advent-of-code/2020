import Data.List.Split
import Control.Monad

countTrees (r,d) = snd . foldl (\(x,c) l -> (x+r, c+(if l!!x == '#' then 1 else 0))) (0,0) . map head . chunksOf d

part1 = countTrees (3, 1)
part2 board = product $ map (`countTrees` board) [(1,1), (3,1), (5,1), (7,1), (1,2)]

main = map cycle . lines <$> readFile "data.txt" >>= \b -> forM_ [part1, part2] $ print . ($ b)
