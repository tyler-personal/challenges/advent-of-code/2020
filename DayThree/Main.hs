{-# LANGUAGE LambdaCase, RecordWildCards #-}
import Data.List.Split

type Row = [Square]
type Board = [Row]

data Square = Open | Tree deriving (Eq, Show)
data Slope = Slope { right :: Int, down :: Int }

mkRow :: String -> Row
mkRow = cycle . map (\case { '.' -> Open; '#' -> Tree })

countTrees :: Board -> Slope -> Int
countTrees board Slope{..} = snd $ foldl parseRow (0,0) board'
  where
    board' = takeEach down board
    parseRow (x, count) row = (x + right, count + (if row !! x == Tree then 1 else 0))

part1 :: Board -> Int
part1 = (`countTrees` Slope 3 1)

part2 :: Board -> Int
part2 board = product counts
  where counts = map (countTrees board) [Slope 1 1, Slope 3 1, Slope 5 1, Slope 7 1, Slope 1 2]

main :: IO ()
main = do
  board <- map mkRow . lines <$> readFile "data.txt"
  print $ part1 board
  print $ part2 board

takeEach :: Int -> [a] -> [a]
takeEach n = map head . chunksOf n
