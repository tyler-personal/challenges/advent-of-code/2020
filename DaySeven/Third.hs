{-# LANGUAGE NoImplicitPrelude, OverloadedStrings, OverloadedLists #-}
import Protolude
import Protolude.Partial ((!!), read, init)
import qualified Data.Map as M
import Data.Text (splitOn, unpack)

type Bags = Map Text [(Int, Text)]

slice :: Int -> Int -> [a] -> [a]
slice start end
  | start < 0 = (`slice` end) =<< (start +) . length
  | end < 0   = slice start =<< (end   +) . length
  | otherwise = take (end - start) . drop  start

createBags :: [Text] -> Bags
createBags lines = flip evalState [] $ do
  forM_ lines $ \line -> do
    let [color, rest] = splitOn " bags contain" line
    when (rest /= " no other bags.") $ do
      let values = [value
            & splitOn " "
            & \(_:x:xs) -> (read (unpack x), unwords (init xs))
            | value <- splitOn "," rest]
      modify (M.insert color values)
  get

hasBag :: Text -> Bags -> Text -> Bool
hasBag searchTerm bags key = case map snd <$> M.lookup key bags of
  (Just values) -> not (null values)
    && searchTerm `elem` values
    || any (hasBag searchTerm bags) values
  Nothing -> False

nestedBagCount :: Bags -> Text -> Int
nestedBagCount bags key = case M.lookup key bags of
  (Just values) -> sum $ map (\(c, v) -> c + (c * nestedBagCount bags v)) values
  Nothing -> 0

part1 :: Bags -> Int
part1 bags = length $ filter (hasBag "shiny gold" bags) (M.keys bags)

part2 :: Bags -> Int
part2 = (`nestedBagCount` "shiny gold")

main :: IO ()
main = do
  bags <- createBags . lines <$> readFile "data.txt"
  print $ part1 bags
  print $ part2 bags
