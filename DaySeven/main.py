from itertools import dropwhile

with open("_data.txt") as f:
    lines = f.read().splitlines()

bags = {}

for line in lines:
    [color, rest, *_] = line.split(" bags contain")
    if rest != " no other bags.":
        values = [" ".join(value.split(" ")[2:-1]) for value in rest.split(",")]
        bags[color] = values

def has_bag(key, search_term):
    try:
        values = bags[key]
        return len(values) > 0 \
            and (search_term in values) \
            or any(has_bag(k, search_term) for k in values)
    except KeyError:
        return False

print(bags)
print(sum(1 for bag in bags if has_bag(bag, "shiny gold")))

