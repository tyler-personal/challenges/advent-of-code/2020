{-# LANGUAGE OverloadedLists, TupleSections, LambdaCase #-}
import Prelude hiding (lookup, null)
import Data.Map (insert, Map(..), null, toList, fromList, lookup)
import Data.List.Split
import Control.Lens
import Data.List (stripPrefix, dropWhileEnd, intercalate)
import Data.Char (isSpace)
import Data.Maybe (mapMaybe)
import Debug.Trace

type Bag = String
type Bags = Map Bag [Bag]

add :: Bags -> String -> Bags
--add bags line = (key, values) : bags -- for the list version
add bags line = insert key values bags
  where
    (key, values') = line
      & splitOn " bags contain "
      & \[k,v] -> (k, v & splitOnAll [" bag, ", " bags, "])

    lastValue = stripSuffix " bag." . stripSuffix " bags." $ last values'
    values = map (trim . dropWhile (/= ' ')) $ init values' ++ [lastValue]

splitOnAll :: Eq a => [[a]] -> [a] -> [[a]]
splitOnAll []            xs = [xs]
splitOnAll [s]           xs = splitOn s xs
splitOnAll [s1,s2]       xs = splitOn s2 . intercalate s2 . splitOn s1 $ xs
splitOnAll (s1:s2:s3:ss) xs = splitOnAll (s3:ss) $ intercalate s2 . splitOn s1 $ xs

trim :: String -> String
trim = dropWhileEnd isSpace . dropWhile isSpace

stripSuffix :: Eq a => [a] -> [a] -> [a]
stripSuffix suffix text = text & reverse
  & stripPrefix (reverse suffix)
  & \case
    (Just x) -> reverse x
    Nothing  -> text

part1 :: Bags -> Int
part1 bags
  | null bags = 0
  | "shiny gold" `elem` v = traceShow k (1 + nextBags)
  | otherwise = subBags + nextBags
  where
    ((k,v):bs) = toList bags
    subBags = part1 . fromList $ mapMaybe (\v' -> (v',) <$> (v' `lookup` bags)) v
    nextBags = part1 $ fromList bs

main :: IO ()
main = do
  input <- foldl add [] . lines <$> readFile "data.txt"
  print input
  writeFile "output.txt" (show input)
  print $ part1 input
  pure ()

