{-# LANGUAGE OverloadedLists #-}
import qualified Data.Map as M
import Data.List.Split
import Control.Monad.State
import Control.Arrow
import Data.Maybe
import Data.Function

type Bags = M.Map String [String]

slice :: Int -> Int -> [a] -> [a]
slice start end
  | start < 0 = (`slice` end) =<< (start +) . length
  | end < 0   = (slice start) =<< (end   +) . length
  | otherwise = (drop  start) >>> take (end - start)

createBags :: [String] -> Bags
createBags lines = flip evalState [] $ do
  forM_ lines $ \line -> do
    let [color, rest] = splitOn " bags contain" line
    when (rest /= " no other bags.") $ do
      let values = [value & splitOn " " & slice 2 (-1) & unwords | value <- splitOn "," rest]
      modify (M.insert color values)
  get

hasBag :: String -> Bags -> String -> Bool
hasBag searchTerm bags key
  | isJust values =
    not (null values)
    && searchTerm `elem` fromJust values
    || any (hasBag searchTerm bags) (fromJust values)
  | otherwise = False
  where values = M.lookup key bags

main :: IO ()
main = do
  bags <- createBags . lines <$> readFile "data.txt"
  print . length $ filter (hasBag "shiny gold" bags) (M.keys bags)
  pure ()
