import Data.String

record Passport where
  constructor MkPassport
  birthYear, issueYear, expirationYear : Maybe Int
  height, hairColor, eyeColor, passportID, countryID : Maybe String

initPassport : Passport
initPassport = MkPassport i i i s s s s s
  where i = Nothing
        s = Nothing

data KeyOrValue = Key | Value

IntermediateData : Type
IntermediateData = (String, String, KeyOrValue, List Passport)

addPassport : String -> String -> Bool -> List Passport -> List Passport
addPassport key value new (p::ps) = ((case key of
  "byr" => record { birthYear = parseInteger value }
  "iyr" => record { issueYear = parseInteger value }
  "eyr" => record { expirationYear = parseInteger value }
  "hgt" => record { height = Just value }
  "hcl" => record { hairColor = Just value }
  "ecl" => record { eyeColor = Just value }
  "pid" => record { passportID = Just value }
  "cid" => record { countryID = Just value }
  ) (if new then initPassport else p)) :: (if new then (p::ps) else ps)

parseChar : IntermediateData -> Char -> IntermediateData
parseChar (k, v, kOrV, res) c = case (kOrV, c) of
  (Key  , ':')  => (k, v, Value, res)
  (Value, ' ')  => (k, v, Key, addPassport k v False res)
  (Value, '\n') => (k, v, Key, addPassport k v True res)
  (Key  , c)    => (k++pack [c], v, Key, res)
  (Value, c)    => (k, v++pack [c], Value, res)

mkData : String -> List Passport
mkData lines = (\(_,_,_,r) => r) $ foldl parseChar ("", "", Key, [initPassport]) (unpack lines)

isValidPart1 : Passport -> Bool
isValidPart1 p = checkFields requiredInts && checkFields requiredStrings
  where
    requiredInts = [birthYear, issueYear, expirationYear]
    requiredStrings = [hairColor, eyeColor, height, passportID]
    checkFields = all (isJust . p)

main : IO ()
main = do
  lines <- readFile "_data.txt"
  case lines of
    Left e => print 4
    Right => print . length $ filter isValidPart1 (mkData lines)

