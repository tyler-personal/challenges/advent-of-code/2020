{-# LANGUAGE LambdaCase, TemplateHaskell, RecordWildCards #-}
import Text.Read
import Control.Lens
import Data.List.Split hiding (endsWith, startsWith)
import Control.Arrow
import Data.Maybe
import Control.Applicative
import Control.Monad

type Key   = String
type Value = String
type PassportData = [(Key, Value)]

data Passport = Passport
  { _birthYear      :: Maybe Int
  , _issueYear      :: Maybe Int
  , _expirationYear :: Maybe Int
  , _countryID      :: Maybe Int
  , _passportID     :: Maybe String
  , _height         :: Maybe String
  , _hairColor      :: Maybe String
  , _eyeColor       :: Maybe String
  } deriving Show
makeLenses ''Passport

mkData :: String -> [PassportData]
mkData = splitOn "\n\n"
  >>> map (splitOneOf " \n" >>> map (splitOn ":" >>> (\case {[k,v] -> (k,v); _ -> ("","")})))
  >>> map (filter (/= ("","")))

initPassport :: Passport
initPassport = Passport n n n n n n n n
  where n = Nothing

mkPassport :: Passport -> PassportData -> Passport
mkPassport passport = \case
  [] -> passport
  ((k,v):xs) -> (let _v = Just (read v) in passport & case k of
    "byr" -> birthYear .~ _v
    "iyr" -> issueYear .~ _v
    "eyr" -> expirationYear .~ _v
    "cid" -> countryID .~ _v
    "pid" -> passportID ?~ v
    "hgt" -> height ?~ v
    "hcl" -> hairColor ?~ v
    "ecl" -> eyeColor ?~ v
    _ -> error "invalid input"
    ) & (`mkPassport` xs)

isValidPart1 :: Passport -> Bool
isValidPart1 p = checkFields requiredInts && checkFields requiredStrings
  where
    requiredInts = [birthYear, issueYear, expirationYear]
    requiredStrings = [hairColor, eyeColor, height, passportID]
    checkFields = all (isJust . (p ^.))

isValidPart2 :: Passport -> Bool
isValidPart2 p@Passport{..} = isValidPart1 p
  && check (`elem` [1920..2002]) _birthYear
  && check (`elem` [2010..2020]) _issueYear
  && check (`elem` [2020..2030]) _expirationYear
  && (checkHeight "cm" [150..193] || checkHeight "in" [59..76])
  && (_hairColor & check
    ((`startsWith` "#")
    ^&& (drop 1 >>> all ((`elem` ['0'..'9']) ^|| (`elem` ['a'..'f'])))
    ^&& (length >>> (== 7))
    ))
  && check (`startsWith` "#") _hairColor
  && check (`elem` ["amb", "blu", "brn", "gry", "grn", "hzl", "oth"]) _eyeColor
  && check (length >>> (== 9)) _passportID
  where
    check = maybe False
    checkHeight unit range = _height & check
      ((`endsWith` unit) ^&& (dropLast 2 >>> readMaybe >>> check (`elem` range)))

main :: IO ()
main = do
  passports <- map (mkPassport initPassport) . mkData <$> readFile "data.txt"
  forM_ [isValidPart1, isValidPart2] $ \p -> print . length $ filter p passports

x ^&& y = liftA2 (&&) x y
x ^|| y = liftA2 (||) x y

startsWith :: Eq a => [a] -> [a] -> Bool
startsWith xs start = take (length start) xs == start

endsWith :: Eq a => [a] -> [a] -> Bool
endsWith xs end = reverse (take (length end) (reverse xs)) == end

dropLast :: Int -> [a] -> [a]
dropLast n = reverse >>> drop n >>> reverse
