import Data.List.Split
import Data.List

part1 :: [String] -> Int
part1 = sum . map (length . nub . filter (/= '\n'))

part2 :: [String] -> Int
part2 = sum . map (length . foldl1 intersect . lines)

main :: IO ()
main = do
  groups <- splitOn "\n\n" <$> readFile "data.txt"
  print $ part1 groups
  print $ part2 groups



