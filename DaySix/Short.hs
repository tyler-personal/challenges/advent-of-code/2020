import Data.List.Split
import Data.List
import Control.Monad

main = (splitOn "\n\n" <$> readFile "data.txt") >>= \g ->
  forM_ [nub . filter (/= '\n'), foldl1 intersect . lines] $ \f ->
    print . sum $ map (length . f) g
