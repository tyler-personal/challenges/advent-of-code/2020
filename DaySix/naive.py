with open("data.txt") as file:
    lines = file.read().splitlines()

group = []
part1_sum = 0
part2_sum = 0

for line in (lines + [""]):
    if line == "":
        part1_answers = set(group[0])
        part2_answers = set(group[0])

        for person in group[1:]:
            for answer in person:
                part1_answers.add(answer)
            part2_answers = part2_answers.intersection(person)

        part1_sum += len(part1_answers)
        part2_sum += len(part2_answers)
        group = []
    else:
        group.append(line)

print(part1_sum)
print(part2_sum)
